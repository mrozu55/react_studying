import React, { Component } from "react";
import NavBar from "./components/navbar";
import Counters from "./components/counters";
import Posts from "./components/posts";
import "./App.css";

class App extends Component {
  state = {
    index: 4,
    counters: [
      { id: 1, value: 4 },
      { id: 2, value: 0 },
      { id: 3, value: 0 },
      { id: 4, value: 0 }
    ]
  };

  handlerIncrement = counter => {
    
    const counters = [...this.state.counters];
    const index = counters.indexOf(counter);
    counters[index] = { ...counter };
    counters[index].value++;
    this.setState({ counters });
  };

  handlerReset = () => {
    const counters = this.state.counters.map(e => {
      e.value = 0;
      return e;
    });
    this.setState({ counters });
  };

  handlerDelete = counterId => {
    //console.log("Event Handelr Called!", counterId);
    const counters = this.state.counters.filter(c => c.id !== counterId);
    this.setState({ counters: counters });
  };

  handleSort = () => {
    const array = [...this.state.counters];
    array.sort((a, b) => {
      if (a.value > b.value) return -1;
      if (a.value < b.value) return 1;
      return 0;
    });

    this.setState({ counters: array });
  };

  handleAdd = () => {
    let counters = [...this.state.counters];
    const newIndex = this.state.index + 1;
    let newObject = { id: newIndex, value: 0 };
    console.log(newObject);
    counters.push(newObject);
    this.setState({ counters: counters, index: newIndex });
  };
  render() {
    return (
      <React.Fragment>
        <NavBar counter={this.state.counters.filter(c => c.value > 0).length} />
        <main className="container d-flex justify-content-around p-5">
          <Counters
            key={this.state.counters.id}
            onReset={this.handlerReset}
            onIncrement={this.handlerIncrement}
            onDelete={this.handlerDelete}
            onSort={this.handleSort}
            onAdd={this.handleAdd}
            counter={this.state.counters}
          />
          <Posts />
        </main>
      </React.Fragment>
    );
  }
}

export default App;
