import React, { Component } from "react";

class Counter extends Component {
  // constructor(props) {
  //   super(props);
  //   this.handlearIncrement = this.handlearIncrement.bind(this);
  // }

  // handlearIncrement(product) {
  //   console.log(product);
  //   this.setState({ value: this.state.value + 1 });
  // }

  render() {
    // this.getBadgeClass();

    return (
      <div className="d-flex flex-column p-2">
        {/* <img
          style={{ width: 600 }}
          className="align-self-center"
          src={this.state.imageUrl}
          alt=""
        /> */}
        <span className={this.getBadgeClass()}>{this.formatCount()}</span>
        <button
          onClick={() => this.props.onIncrement(this.props.counter)}
          className="btn btn-secondary btn-sm align-self-center"
        >
          Increment
        </button>
        <button
          onClick={() => this.props.onDelete(this.props.counter.id)}
          className="btn btn-danger btn-sm m-2 align-self-center"
        >
          Delete
        </button>
      </div>
    );
  }

  getBadgeClass() {
    let classes = "align-self-center badge m-2 badge-";
    classes += this.props.counter.value === 0 ? "warning" : "primary";
    return classes;
  }

  formatCount() {
    const { value } = this.props.counter;
    return value === 0 ? "Zero" : value;
  }

  // metodaNitrizenki() {
  //   if (this.state.tags.length === 0) return <p>Nie ma tagów!</p>;

  //   return (
  //     <ul>
  //       {this.state.tags.map(tag => (
  //         <li> {tag} </li>
  //       ))}
  //     </ul>
  //   );
  // }

  // container() {
  //   return (
  //     <div className="d-flex flex-column p-2">
  //       <img
  //         style={{ width: 600 }}
  //         className="align-self-center"
  //         src={this.state.imageUrl}
  //         alt=""
  //       />
  //       <span className={this.getBadgeClass()}>{this.formatCount()}</span>
  //       <button className="btn btn-secondary btn-sm align-self-center">
  //         Increment
  //       </button>
  //     </div>
  //   );
  // }
}

export default Counter;
