import React, { Component } from "react";
import Post from "./post";

class Posts extends Component {
  state = {
    index: 0,
    value: "",
    autor: "Admin",
    posts: [
      {
        id: 0,
        autor: this.autor,
        content: "Hello World!",
        time: new Date().toLocaleTimeString(),
        date: new Date().toLocaleDateString()
      }
    ]
  };

  constructor() {
    super();
    this.state.index = this.state.posts.length;
  }

  //Enter can be used for submitting new post
  handleKeyPress = event => {
    if (event.key === "Enter") {
      this.handleSubmit();
    }
  };

  handleChange = event => {
    this.setState({ value: event.target.value });
  };

  handleSubmit = () => {
    if (!(this.state.value === "")) {
      let posts = [...this.state.posts];
      console.log(this.state.index);
      console.dir(new Date());
      console.log(new Date().getDate());
      console.log(new Date().toDateString());
      const index = this.state.index + 1;
      const newPost = {
        id: index,
        autor: this.state.autor,
        content: this.state.value,
        time: new Date().toLocaleTimeString(),
        date: new Date().toLocaleDateString()
      };
      posts.push(newPost);
      this.setState({ posts: posts, index: index, value: "" });
    }
  };

  render() {
    return (
      <div>
        <div className="form-group">
          <label form="Post">Type your post</label>
          <input
            type="text"
            className="form-control"
            value={this.state.value}
            onChange={this.handleChange}
            onKeyPress={this.handleKeyPress}
          />

          <input
            type="submit"
            value="Submit"
            className="form-control"
            onClick={this.handleSubmit}
            className="btn btn-primary btn-sm m-2"
          />
        </div>
        {this.state.posts
          .map(post => <Post key={post.id} post={post} />)
          .reverse()}
      </div>
    );
  }
}

export default Posts;
