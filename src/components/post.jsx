import React, { Component } from "react";

class Post extends Component {
  render() {
    return (
      <div className="mb-2">
        <div className="d-flex justify-content-between border border-secondary rounded p-2 w-100">
          <span>{this.props.post.content}</span>

          <div className="d-flex flex-column justify-content-end badge badge-pill badge-primary">
            <span className="">{this.props.post.time}</span>
            <span className="">{this.props.post.date}</span>
          </div>
        </div>
      </div>
    );
  }
}

export default Post;
