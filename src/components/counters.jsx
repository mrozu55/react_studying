import React, { Component } from "react";
import Counter from "./counter";

class Counters extends Component {
  // state = {
  //   counters: [
  //     { id: 1, value: 4 },
  //     { id: 2, value: 0 },
  //     { id: 3, value: 0 },
  //     { id: 4, value: 0 }
  //   ]
  // };

  // handlerIncrement = counter => {
  //   console.log(counter);
  //   const counters = [...this.state.counters];
  //   const index = counters.indexOf(counter);
  //   counters[index] = { ...counter };
  //   counters[index].value++;
  //   this.setState({ counters });
  // };

  // handlerReset = () => {
  //   const counters = this.state.counters.map(e => {
  //     e.value = 0;
  //     return e;
  //   });
  //   this.setState({ counters });
  // };

  // handlerDelete = counterId => {
  //   //console.log("Event Handelr Called!", counterId);
  //   const counters = this.state.counters.filter(c => c.id !== counterId);
  //   this.setState({ counters: counters });
  // };

  render() {
    const { onReset, onIncrement, onDelete, onSort, onAdd } = this.props;
    return (
      <div>
        <button onClick={onReset} className="btn btn-primary btn-sm m-2">
          Reset
        </button>
        <button onClick={onSort} className="btn btn-primary btn-sm m-2">
          Sort
        </button>
        <button onClick={onAdd} className="btn btn-primary btn-sm m-2">
          Add
        </button>
        {this.props.counter.map(counter => (
          <Counter
            key={counter.id}
            onDelete={onDelete}
            onIncrement={onIncrement}
            counter={counter}
          />
        ))}
      </div>
    );
  }
}

export default Counters;
